describe('Navigation through pages', function () {

    before(() => {
        cy.clearLocalStorageCache();
    });

    beforeEach(() => {
        cy.restoreLocalStorageCache();
    });

    afterEach(() => {
        cy.saveLocalStorageCache();
    });

    it('Got to the top', function () {
        // SignUp
        cy.visit('/')
        cy.signUpUser('test@test.test')
        // Go to top from home
        cy.scrollTo('bottom')
        cy.get('[name="totop"]').click()
        // Go to top from current user archives
        cy.visit('/archive')
        cy.scrollTo('bottom')
        cy.get('[name="totop"]').click()
        // Go to top from user 'startlight' archive
        cy.visitArchive('startlight@test.test')
        cy.scrollTo('bottom')
        cy.get('[name="totop"]').click()
    })

    it('Load more posts', function () {
        cy.visit('/')
        // Load more random posts from home
        cy.get('[name="loadMore"]').click()
        // Load more disabled in current user archives
        cy.visit('/archive')
        cy.get('[name="loadMore"]').should('be.disabled')
        // Load more posts from user 'startlight' archive
        cy.visitArchive('startlight@test.test')
        cy.get('[name="loadMore"]').click()
    })   

    it('Access current user archive', function(){
        // From home
        cy.visit('/')
        cy.get('[name="accessArchive"]').click()
        cy.url().should('contain', '/archive')
        // From user 'startlight' archive
        cy.visitArchive('startlight@test.test')
        cy.get('[name="accessArchive"]').click()
        cy.url().should('contain', '/archive')
    })

    it('Delete test account', function () {
        // Delete account
        cy.visit('/user/details')
        cy.get('button[name="deleteAccount"]').click()
        cy.get('button[name="confirmDelete"]').click()
        cy.wait(2000)
    })

})