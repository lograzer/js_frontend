describe('Galleries posts navigation', function () {

    before(() => {
        cy.clearLocalStorageCache();
    });

    beforeEach(() => {
        cy.restoreLocalStorageCache();
    });

    afterEach(() => {
        cy.saveLocalStorageCache();
    });

    it('Access random post details', function () {
        cy.visit('/')
        cy.signUpUser('test@test.test')
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
    })

    it('Access random post author archive', function () {
        cy.visit('/')
        cy.get('a.user-archive').first().click({ force: true })
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
    })

    it('Like/Dislikes a post from home', function () {
        cy.visit('/')
        // Like
        cy.get('button.MuiIconButton-root').first().click({ force: true })
        // Dislike
        cy.get('button.MuiIconButton-root').first().click({ force: true })
    })

    it('Like/Dislikes a post from random user archive', function () {
        cy.visit('/')
        cy.get('a.user-archive').first().click({ force: true })
        // Like
        cy.get('button.MuiIconButton-root').first().click({ force: true })
        // Dislike
        cy.get('button.MuiIconButton-root').first().click({ force: true })
    })   

    it('Add a post - fail', function () {
        // From Home
        cy.visit('/')
        cy.get('[name="createPost"]').click()
        // Cancel creation
        cy.get('[name="cancelCreate"]').click()
        cy.url().should('contain', '/')
        cy.get('[name="createPost"]').click()
        // Form creation
        cy.get('input[name="title"]')
            .type('Postit')
            .should('have.value', 'Postit');
        cy.get('textarea[name="description"]')
            .type('Voila un bien joli post. Avec une description très complète.')
            .should('have.value', 'Voila un bien joli post. Avec une description très complète.')
        cy.get('form').submit()
        cy.get('[name="error"]').should('contain', 'Please choose a picture.')

    })

    it('Add a post - success', function () {
        // From Home
        cy.visit('/')
        cy.get('[name="createPost"]').click()
        // Form creation
        cy.get('input[name="title"]')
            .type('Postit')
            .should('have.value', 'Postit');
        cy.get('textarea[name="description"]')
            .type('Voila un bien joli post. Avec une description très complète.')
            .should('have.value', 'Voila un bien joli post. Avec une description très complète.')
        // Upload file using drag and drop with a fixture
        const fileName = 'pictures/tarte.jpg';
        cy.fixture(fileName, 'binary')
            .then(Cypress.Blob.binaryStringToBlob)
            .then((fileContent) =>
                cy.get('input[type=file]').upload({
                    fileContent,
                    fileName,
                    mimeType: 'image/jpg',
                    encoding: 'utf8'
                })
            );
        cy.get('form').submit()
        // Verify result
        cy.visit('/archive')
        cy.get('a.post-image').first().click({ force: true })
        cy.get('[name="title"]').should('contain', 'Postit')
    })

    it('Edit a post', function () {
        cy.visit('/archive')
        // Edit the only one post by clicking second icon button
        cy.get('a.MuiIconButton-root').first().click({ force: true })
        cy.url().should('contains', '/edit/')
        // Cancel edit
        cy.get('button[name="cancelEdit"]').click()
        cy.url().should('contain', '/archive')
        // Edit post
        cy.get('a.MuiIconButton-root').first().click({ force: true })
        cy.get('input[name="title"]')
            .should('have.value', 'Postit');
        cy.get('input[name="title"]')
            .clear()
            .type('Post-it')
            .should('have.value', 'Post-it')
        cy.get('textarea[name="description"]')
            .should('have.value', 'Voila un bien joli post. Avec une description très complète.');
        cy.get('form').submit()

        // Check url
        cy.url().should('contain', '/archive')
        // Verify changes on post details
        cy.get('a.post-image').first().click({ force: true })
        cy.get('[name="title"]').should('contain', 'Post-it')
    })

    it('Delete a post', function () {
        cy.visit('/archive')
        // Remove the only one post by clicking the first icon button
        cy.get('button.MuiIconButton-root').first().click({ force: true })
        cy.get('[name="noPosts"]').should('exist')
    })

    it('Delete test account', function () {
        // Delete account
        cy.visit('/user/details')
        cy.get('button[name="deleteAccount"]').click()
        cy.get('button[name="confirmDelete"]').click()
        cy.wait(2000)
    })

})