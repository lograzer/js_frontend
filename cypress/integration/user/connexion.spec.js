
describe('Account connexion', function () {
  before(() => {
    cy.clearLocalStorageCache();
  });

  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });

  afterEach(() => {
    cy.saveLocalStorageCache();
  });

  it('Fail log in', function () {
    // Get page
    cy.visit('/')
    // Submit form 
    cy.get('input[name="email"]')
      .type('wrongName@test.test')
      .should('have.value', 'wrongName@test.test');
    cy.get('input[name="password"]')
      .type('root')
      .should('have.value', 'root')
    cy.get('form').submit()
    cy.get('[name="error"]').should('contain', 'Email or password incorrect.')
  })

  it('Success log in', function () {
    // Get page
    cy.visit('/')
    // Submit form 
    cy.loginUser('loulou@test.test')
  })

  it('Success log out', function () {
    cy.getLocalStorage('token').should('exist');
    // Get details page
    cy.visit('/user/details')
    // Sign out
    cy.get('button[name="signout"]').click()
    // Return to sign in page
    cy.url().should('contain', '/signin')
    cy.clearLocalStorage()
  })


})