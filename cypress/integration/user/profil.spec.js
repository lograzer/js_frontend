describe('User profil', function () {

    before(() => {
        cy.clearLocalStorageCache();
    });

    beforeEach(() => {
        cy.restoreLocalStorageCache();
    });

    afterEach(() => {
        cy.saveLocalStorageCache();
    });

    it('Create test account', function () {
        // Create account
        cy.visit('/')
        cy.signUpUser('test@test.test')
    })

    it('Fail to access user details', function () {
        // Missing token before accessing profil
        cy.clearLocalStorageCache()
        cy.visit('/user/details')
        cy.url().should('not.contain', '/user/details')
    })

    it('Edit profil details', function () {
        // Log in user
        cy.visit('/')
        cy.loginUser('test@test.test')
        // Cancel edit
        cy.visit('/user/edit')
        cy.get('button[name="goBack"]').click()
        cy.url().should('contain', '/archive')
        // Edit profil
        cy.visit('/user/edit')
        cy.get('input[name="pseudo"]')
            .should('have.value', 'test');
        cy.get('input[name="pseudo"]')
            .clear()
            .type('SuperPseudo')
            .should('have.value', 'SuperPseudo')
        // TODO : add picture
        cy.get('form').submit()
        // Check url
        cy.url().should('contain', '/archive')
        // Verify changes
        cy.visit('/user/details')
        cy.get('[name="pseudo"]').should('contain', 'SuperPseudo')
    })

    it('Edit profil with missing token', function () {
        cy.visit('/user/edit')
        // Edit profil
        cy.visit('/user/edit')
        cy.clearLocalStorageCache()
        cy.get('form').submit()
        // Check url
        cy.url().should('contain', '/')
        cy.getLocalStorage('token').should('not.exist');
    })

    it('Delete test account', function () {
        // Login user
        cy.visit('/')
        cy.loginUser('test@test.test')
        // Delete account
        cy.visit('/user/details')
        cy.get('button[name="deleteAccount"]').click()
        cy.get('button[name="confirmDelete"]').click()
    })

})