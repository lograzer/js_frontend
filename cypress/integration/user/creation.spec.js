describe('Account creation', function () {

  before(() => {
    cy.clearLocalStorageCache();
  });

  beforeEach(() => {
    cy.restoreLocalStorageCache();
  });

  afterEach(() => {
    cy.saveLocalStorageCache();
  });

  it('Get sign up form', function () {
    cy.visit('/')
    cy.get('button[name="changeForm"]')
      .should('have.text', 'New member ? Join Us !');
    // Change form 
    cy.get('button[name="changeForm"]').click()
    cy.get('button[name="changeForm"]')
      .should('have.text', 'Connect to my account.');
  })

  it('Fail sign up', function () {
    // Get page
    cy.visit('/')
    // Submit form 
    cy.get('button[name="changeForm"]').click()
    cy.get('input[name="email"]')
      .type('loulou@test.test')
      .should('have.value', 'loulou@test.test');
    cy.get('input[name="password"]')
      .type('root')
      .should('have.value', 'root')
    cy.get('form').submit()
    cy.get('[name="error"]').should('contain', 'This email address is already taken. Choose another one.')

  })

  it('Success sign up', function () {
    // Get page
    cy.visit('/')
    cy.getLocalStorage('token').should('not.exist');
    // Submit form 
    cy.signUpUser('toDelete@test.test')
  })

  it('Archive of the new created account', function () {
    cy.visit('/archive')
    cy.get('[name="noPosts"]').should('contain', 'No post to display.')
  })

  it('User details of the new created account', function () {
    cy.visit('/user/details')
    cy.get('[name="pseudo"]').should('contain', 'toDelete')
    cy.get('[name="contact"]').should('contain', 'Contact: toDelete@test.test')
    cy.get('[name="nbPublications"]').should('contain', 'Number of publications: 0')
  })

  it('Fail delete account with missing token', function () {
    
    cy.visit('/user/details')
    cy.get('button[name="deleteAccount"]').click()
    cy.get('button[name="confirmDelete"]').click()
    cy.get('button[name="confirmCancel"]').click()
    cy.url().should('contain', '/signin')
  })

  it('Success delete account', function () {
    // cy.visit('/')
    // cy.loginUser('toDelete@test.test')
    // Get detail page
    cy.visit('/user/details')
    cy.get('button[name="deleteAccount"]').click()
    // Rollback delete
    cy.get('button[name="confirmCancel"]').click()
    // Confirm delete
    cy.get('button[name="deleteAccount"]').click()
    cy.get('button[name="confirmDelete"]').click()
    // Check url
    cy.url().should('contain', '/')
  })

})