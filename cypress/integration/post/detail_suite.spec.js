describe('Galleries posts navigation', function () {

    before(() => {
        cy.clearLocalStorageCache();
    });

    beforeEach(() => {
        cy.restoreLocalStorageCache();
    });

    afterEach(() => {
        cy.saveLocalStorageCache();
    });


    it('Load more likes', function () {
        // Log in
        cy.visit('/')
        cy.loginUser('loulou@test.test')
        cy.visit('/archive')
        // Display post with lot of likes
        cy.get('a.post-image').first().click({ force: true })
        cy.get('[name="title"]').should('contain', 'Couscous party')
        // Load more likes
        cy.get('[name="displayLikes"]').should('not.be.checked').click().wait(500)
        cy.get('[name="loadMoreLikes"]').should('not.be.disabled').click()
        cy.get('[name="displayLikes"]').should('be.checked')
        // Log out
        cy.visit('/user/details')
        cy.get('button[name="signout"]').click()
        cy.wait(2000)
    })

})