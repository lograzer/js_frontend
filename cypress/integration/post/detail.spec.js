describe('Galleries posts navigation', function () {

    before(() => {
        cy.clearLocalStorageCache();
    });

    beforeEach(() => {
        cy.restoreLocalStorageCache();
    });

    afterEach(() => {
        cy.saveLocalStorageCache();
    });

    it('Like a random post', function () {
        // Sign up to test account
        cy.visit('/')
        cy.signUpUser('test@test.test')
        // Like
        cy.get('button.MuiIconButton-root').first().click({ force: true })
    })

    it('Display my like from a random post details', function () {
        // Display post details  
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
        // Display likes
        cy.get('[name="displayLikes"]').click()
        // Verify result
        cy.get('div[name="likesZone"]').should('contain', 'test liked this post')
        // Hide likes
        cy.get('[name="displayLikes"]').click()
    })

    it('Display likes with missing token', function () {
        // Remove token
        cy.clearLocalStorageCache()
        // Display likes
        cy.get('[name="displayLikes"]').click()
        // Verify result
        cy.get('div[name="likesZone"]').should('contain', 'No like.')
    })

    it('Add comment to random post details', function () {
        // Login user
        cy.visit('/')
        cy.loginUser('test@test.test')
        // Display post details  
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
        // Add my comment
        cy.get('textarea[name="commentText"]')
            .type('Je m\'exprime sur ce post ! :D')
            .should('have.value', 'Je m\'exprime sur ce post ! :D');
        cy.get('form').submit()
        // Verify result
        cy.get('textarea[name="commentText"]')
            .should('have.value', '');
        cy.contains('Je m\'exprime sur ce post ! :D')
        // Load more comments disabled
        cy.get('[name="loadMoreComments"]').should('be.disabled')
    })

    it('Delete comment from random post details', function () {
        // Delete my comment
        cy.get('button.MuiIconButton-sizeSmall').first().click({ force: true })
        cy.get('div[name="commentsZone"]').should('not.contain', 'test Je m\'exprime sur ce post ! :D')
    })

    it('Add comment with missing token', function () {
        // Remove token
        cy.clearLocalStorageCache()
        // Add my comment
        cy.get('textarea[name="commentText"]')
            .type('Ce commentaire ne doit pas être ajouté !')
            .should('have.value', 'Ce commentaire ne doit pas être ajouté !');
        cy.get('form').submit()
        // Verify result
        cy.get('[name="error"]').should('contain', 'An error occured. Please retry or verify your internet connection.');
        cy.get('div[name="commentsZone"]').should('contain', 'No comment.')

    })

    it('Delete comment with missing token', function () {
        // Login user
        cy.visit('/')
        cy.loginUser('test@test.test')
        // Display post details  
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
        // Add my comment
        cy.get('textarea[name="commentText"]')
            .type('Ce commentaire ne sera pas supprimé')
            .should('have.value', 'Ce commentaire ne sera pas supprimé');
        cy.get('form').submit()
        cy.wait(2000)
        // Delete my comment
        cy.clearLocalStorageCache()
        cy.get('button.MuiIconButton-sizeSmall').first().click({ force: true })
        cy.get('div[name="commentsZone"]').should('not.contain', 'test Ce commentaire ne sera pas supprimé')
        cy.get('[name="error"]').should('contain', 'An error occured. Please retry or verify your internet connection.')
    })

    it('Add/Delete & Load many comments to random post details', function () {
        // Login user
        cy.visit('/')
        cy.loginUser('test@test.test')
        // Display post details  
        cy.get('a.post-image').first().click({ force: true })
        cy.url().should('contain', '/post/')
        // Add my comments
        var arr = Array.from({ length: 10 }, (v, k) => k + 1)
        cy.wrap(arr).each((index) => {
            cy.get('textarea[name="commentText"]')
                .type('Mon commentaire : ' + index)
            cy.get('form').submit()
            cy.wait(1000)
        })
        // Load more comments disabled
        cy.get('[name="loadMoreComments"]').should('not.be.disabled').click()
        // Verify result (last comment is visible)
        cy.get('div[name="commentsZone"]').should('contain', 'Mon commentaire : 10')
        // Remove all comments
        cy.wrap(arr).each((index) => {
            cy.get('button.MuiIconButton-sizeSmall').first().click({ force: true }).wait(1000)
        })

    })

    it('En large picture', function () {
        // Open picture
        cy.get('button.MuiIconButton-root').first().click({ force: true })
    })

    it('Delete test account', function () {
        // Delete account
        cy.visit('/user/details')
        cy.get('button[name="deleteAccount"]').click()
        cy.get('button[name="confirmDelete"]').click()
        cy.wait(2000)
        cy.clearLocalStorageCache()
    })

})