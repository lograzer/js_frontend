// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import "cypress-localstorage-commands"
import arrayUsers from '../fixtures/user/users.json'

// Custom commands to manage localstorage for cypress tests
let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add("saveLocalStorageCache", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorageCache", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});

Cypress.Commands.add("clearLocalStorageCache", () => {
  localStorage.clear();
  LOCAL_STORAGE_MEMORY = {};
});
const clear = Cypress.LocalStorage.clear

Cypress.LocalStorage.clear = function (keys, ls, rs) {
  // do something with the keys here
  if (keys) {
    return clear.apply(this, arguments)
  }
}
// Custom shortcut methods for login and sign up tests
Cypress.Commands.add("loginUser", (email) => {
  cy.get('input[name="email"]')
    .type(email)
  cy.get('input[name="password"]')
    .type('root')
  cy.get('form').submit()
  cy.get('[name="introduction"]').should('contain', 'Check, like and explore the galleries of the other InstaZZers ! Find inspiration or share your experience with the community.')
});

Cypress.Commands.add("signUpUser", (email) => {
  cy.get('button[name="changeForm"]').click()
  cy.get('input[name="email"]')
    .type(email)
    .should('have.value', email);
  cy.get('input[name="password"]')
    .type('root')
    .should('have.value', 'root')
  cy.get('form').submit()
  cy.get('[name="introduction"]').should('contain', 'Check, like and explore the galleries of the other InstaZZers ! Find inspiration or share your experience with the community.')
});

// Custom finding-key function for json objects
function filterById(jsonObject, email) { return jsonObject.filter(function (jsonObject) { return (jsonObject['email'] == email); })[0]; }

Cypress.Commands.add("visitArchive", (email) => {
  const url = '/user/' + filterById(arrayUsers['users'], email)._id + '/archive'
  cy.visit(url)
})

// Custom command to handle file upload using react-dropzone
Cypress.Commands.add('upload', { prevSubject: 'element', }, (subject, file, fileName) => {

  cy.window().then(window => {
    // line below could maybe be refactored to make use of Cypress.Blob.base64StringToBlob, instead of this custom function.
    // inspired by @andygock, please refer to https://github.com/cypress-io/cypress/issues/170#issuecomment-389837191
    const blob = b64toBlob(file, '', 512)
    // Please note that we need to create a file using window.File,
    // cypress overwrites File and this is not compatible with our change handlers in React Code
    const testFile = new window.File([blob], fileName)
    cy.wrap(subject).trigger('drop', {
      dataTransfer: { files: [testFile] },
    })
  })
})

// Custom convertion method from @nrutman: https://github.com/cypress-io/cypress/issues/170
function b64toBlob(b64Data, contentType = '', sliceSize = 512) {
  const byteCharacters = atob(b64Data)
  const byteArrays = []

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize)

    const byteNumbers = new Array(slice.length)
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i)
    }

    const byteArray = new Uint8Array(byteNumbers)

    byteArrays.push(byteArray)
  }

  const blob = new Blob(byteArrays, { type: contentType })
  return blob
}