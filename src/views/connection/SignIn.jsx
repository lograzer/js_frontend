import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import SignInForm from './SignInForm';
import Home from '../posts/Home';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import { getServerUrl } from '../../utils/Util';

/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  }
}));

/**
 * Pure fonctionnal component in charge of displaying the log in/ subscribe full page   
 * @param {*parametres} props 
 */
export default function SingIn(props) {

  const classes = useStyles();
  const error_default_mesg = "An error occured. Please retry or verify you internet connection.";
  const [showing, setShow] = useState(false);
  const [error, setError] = useState(false);
  const [error_msg, SetErrorMesg] = useState("");
  const join_message = "New member ? Join Us !";
  const connect_message = "Connect to my account.";
  const description_str = "Join the InstaZzers community, a network of professional and amateur photographers.";


  /**
   * Function : Send log in informations and save api token
   */
  const signIn = async (email, password) => {
    try {
      await axios.post(getServerUrl() + "/users/login", {
        email,
        password,
      })
        .then((response) => {
          // Everything is fine with the request
          setError(false);
          localStorage.setItem("token", "Token " + response.data.token);
          props.handleLogin();
          props.history.push('/');

        }, (error) => {
          SetErrorMesg(error_default_mesg);
          setError(true);
          if (error.response) {
            // The request was made and the server responded with a status code 'error.response'
            let mesg = error_default_mesg;
            if (error.response.status === 400) {
              mesg = error.response.data.message;
            }
            SetErrorMesg(mesg);

          } 
        });

    }
    catch (err) {
      // display error messg on form page
      setError(true);
      SetErrorMesg(error_default_mesg);
    }
  };

  /**
   * Function : Send subscribe informations and save api token
   */
  const subscribe = async (email, password) => {
    try {
      await axios.post(getServerUrl() + "/users", {
        email,
        password,
      })
        .then((response) => {
          // Everything is fine with the request
          setError(false);
          localStorage.setItem("token", "Token " + response.data.token);
          props.handleLogin();
          props.history.push('/');

        }, (error) => {
          SetErrorMesg(error_default_mesg);
          setError(true);
          if (error.response) {
            // The request was made and the server responded with a status code 'error.response'
            let mesg = error_default_mesg;
            if (error.response.status === 422) {
              mesg = error.response.data.message;
            }
            SetErrorMesg(mesg);

          } 
        });

    } catch (err) {
      // display error messg on form page
      setError(true);
      SetErrorMesg(error_default_mesg);
    }
  };

  return (
    <div>
      {
        props.tokenExists
          ? <Home />
          :
          <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
              <div>
                <Button color="secondary" name="changeForm" onClick={() => setShow(!showing)}>
                  {showing
                    ? connect_message
                    : join_message
                  }
                </Button>
                {
                  error
                    ? <div><Alert name="error" severity="error">{error_msg}</Alert></div>
                    : <div />
                }
                {showing
                  ? <SignInForm title={join_message} message="Subscribe" myFunction={subscribe}></SignInForm>
                  : <SignInForm title="Sign in" message="Sign in" myFunction={signIn}></SignInForm>
                }
              </div>
              <div className="text-center mb-3 pr-3 pl-3">
                <h5>{description_str}</h5>
              </div>
            </Grid>
          </Grid>
      }
    </div>

  );
}
