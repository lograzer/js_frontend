import React, { useEffect, useState } from "react";
import axios from 'axios';
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import { getServerUrl } from '../../utils/Util';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';
import parseJwt, { tokenInvalid } from '../../utils/Util';

/**
 * Style of the component
 */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
  },
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

/**
 * Pure fonctionnal component in charge of displaying post comment   
 * @param {*parametres} props 
 */
export default function CommentDetails(props) {

  const classes = useStyles();
  const [comments, setComments] = useState([]);
  const limit_comment = 10;
  const [start_comment, setStartComment] = useState(0);
  const [disable_comment, setDisableComment] = useState(false);

  /**
    * Run only after an initial render or after an update on variable
    */
  useEffect(() => {
    getComments();
  }, []);

  /**
   * Api call : get comments of this post
   */
  const getComments = async () => {
    try {
      const access_token = localStorage.getItem("token");
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/posts/" + props.id + "/comments?limit=" + limit_comment + "&start=" + start_comment
      };
      await axios(options)
      .then((res) => {
        setStartComment(start_comment + limit_comment);
        setComments(comments.concat(res.data.comments));
        if (res.data.comments.length === 0 || res.data.comments.length < limit_comment) {
          setDisableComment(true);
        }
      },
        (error) => {
          if (error.response) {
            // The request was made and the server responded with a status code 'error.response'
            if (error.response.status === 401) {
              tokenInvalid();
            }
          }
          else {
            props.setError(true);
          }
        });
           
    } catch (error) { 
      props.setError(true);
    }
  }

  /**
     * Api call : Load more comment
     */
  const loadMoreComment = async () => {
    getComments();
  }

  /**
    * Api call : delete a specific comment
    * @param {*} commentId 
    */
  const deleteComment = async (commentId) => {
    try {
      const access_token = localStorage.getItem("token");
      await axios.delete(getServerUrl() + "/posts/" + props.id + "/comments/" + commentId,
        {
          headers: {
            Authorization: access_token,
            "content-type": "application/json"
          }
        })
        .then((res) => { },
          (error) => {
            if (error.response) {
              // The request was made and the server responded with a status code 'error.response'
              if (error.response.status === 401) {
                tokenInvalid();
                props.setError(true)
              }
            }
            else{
              props.setError(true);
            }
          });
    }
    catch (err) {       
      props.setError(true);
    }
  }

  /**
   * Handle deletion of the selected comment
   * @param {*} commentId 
   */
  const submitDeleteComment = async (commentId) => {
    await deleteComment(commentId);
    var filteredComments = comments.filter(function (c) {
      return c._id !== commentId;
    });
    setComments(filteredComments);
  }

  /**
   * Check if current user is also the author of the post or of the selected comment
   * @param {*} comment_author_id 
   */
  const isCurrentUser = (comment_author_id) => {
    const access_token = localStorage.getItem("token");
    if(access_token){
      return parseJwt(access_token).id === comment_author_id || parseJwt(access_token).id === props.author;
    }
    else{
      return false;
    }
  }

  return (
    <div name="commentsZone" className={classes.root}>
      <Paper className={classes.paper}>
        {comments.length === 0
          ? <p>No comment.</p>
          : Array.from(comments).map(comment => (
            <Grid container wrap="nowrap" spacing={2}>
              <Grid item>
                <Avatar alt={comment.author.pseudo} src={comment.author.avatar} className={classes.large} />
              </Grid>
              <Grid item xs>
                {
                  isCurrentUser(comment.author._id) ?
                    <Grid container spacing={3}>
                      <Grid item xs={12} sm={10}>
                        {
                          comment.author.pseudo ? <Typography><b>{comment.author.pseudo}</b></Typography> : <Typography><b>Unknown</b></Typography>
                        }
                        <Typography>{comment.message}</Typography>
                      </Grid>
                      <Grid item xs={12} sm={2}>
                        <IconButton aria-label="Delete" onClick={() => submitDeleteComment(comment._id)} className={classes.delete} size="small">
                          <Delete />
                        </IconButton>
                      </Grid>
                    </Grid>
                    :
                    <div>
                      {
                        comment.author.pseudo ? <Typography><b>{comment.author.pseudo}</b></Typography> : <Typography><b>Unknown</b></Typography>
                      }
                      <Typography>{comment.message}</Typography>
                    </div>
                }
              </Grid>
            </Grid>
          ))
        }
      </Paper>
      {/* Load more comments */}
      <Grid container justify="center" >
        <Grid item>
          <Button name="loadMoreComments" justify="center" className={classes.loader} variant="contained" color="secondary" size="large" disabled={disable_comment} onClick={loadMoreComment}>
            See more
            </Button>
        </Grid>
      </Grid>
    </div>
  );
}
