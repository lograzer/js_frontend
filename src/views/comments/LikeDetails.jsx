import React, { useEffect, useState } from "react";
import axios from 'axios';
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Button from '@material-ui/core/Button';
import { getServerUrl, tokenInvalid } from '../../utils/Util';

/**
 * Style of the component
 */
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
  },
  paper: {
    padding: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

/**
 * Pure fonctionnal component in charge of displaying a post comment.   
 * @param {*parametres} props 
 */
export default function LikeDetails(props) {

  const classes = useStyles();

  const [likes, setLikes] = useState([]);
  const limit_like = 10;
  const [start_like, setStartLike] = useState(0);
  const [disable_like, setDisableLike] = useState(false);

  /**
   * Run only after an initial render or after an update on variable
   */
  useEffect(() => {
    getLikes();
  }, []);

  /**
   * Api call : get likes of this post
   */
  const getLikes = async () => {
    try {
      const access_token = localStorage.getItem("token");
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/posts/" + props.id + "/likes?limit=" + limit_like + "&start=" + start_like
      };
      await axios(options)
      .then((res)=>{
        setStartLike(start_like + limit_like);
        setLikes(likes.concat(res.data.likes));
        if (res.data.likes.length === 0 || res.data.likes.length < limit_like) {
          setDisableLike(true);
        }
      },
      (error) =>{
        if (error.response) {
          // The request was made and the server responded with a status code 'error.response'
          if (error.response.status === 401) {
            tokenInvalid();
          }
        }
        else{
          props.setError(true);
        }
      });   

    } catch (error) {
      props.setError(true);
    }
  }

  /**
    * Api call : Load more like
    */
  const loadMoreLike = async () => {
    getLikes();
  }

  return (
    <div name="likesZone" className={classes.root}>
      <Paper className={classes.paper}>
        {likes.length === 0
          ? <p>No like.</p>
          : Array.from(likes).map(like => (
            <Grid container wrap="nowrap" spacing={2}>
              <Grid item>
                <Avatar alt={like.pseudo} src={like.avatar} className={classes.large} />
              </Grid>
              <Grid item xs>
                <Typography><b>
                  {
                    like.pseudo ? like.pseudo : "Unknown"
                  }
                </b> liked this post.</Typography>
              </Grid>
            </Grid>
          ))
        }
      </Paper>
      {/* Load more comments */}
      <Grid container justify="center" >
        <Grid item>
          <Button name="loadMoreLikes" justify="center" className={classes.loader} variant="contained" color="secondary" size="large" disabled={disable_like} onClick={loadMoreLike}>
            See more
            </Button>
        </Grid>
      </Grid>
    </div>
  );
}
