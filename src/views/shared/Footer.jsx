import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
  footer: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    color: 'white',
    padding: theme.spacing(2),
    marginTop: theme.spacing(5),
    bottom: 0,
    right: 0,
    left: 0
  }
}));

/**
 * Pure fonctionnal component in charge of the diplsay of the footer
 */
function Footer() {
  const classes = useStyles();

    return (
      <footer className={classes.footer}>
          <Typography variant="h6" align="center" gutterBottom>
            InstaZZ Corp
          </Typography>
          <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
          © InstaZZ Association - 2020
          </Typography>        
      </footer>
    );
}

export default Footer;
