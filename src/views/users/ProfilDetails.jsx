
import React, { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import { Paper } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Grid from '@material-ui/core/Grid';
import Delete from '@material-ui/icons/Delete';
import ExitToApp from '@material-ui/icons/ExitToApp';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Alert from '@material-ui/lab/Alert';
import parseJwt, { getServerUrl, tokenInvalid, getErrorMesg } from '../../utils/Util';

/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
  postContent: {
    padding: theme.spacing(2),
    color: 'black',
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  },
  paper: {
    padding: theme.spacing(2),
    marginTop: theme.spacing(5),
    margin: "auto",
    maxWidth: 900
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  image: {
    width: 128,
    height: 128
  }
}));

/**
 * Pure fonctionnal component in charge of displaying user profil details
 * @param {*} props 
 */
export default function ProfilDetails(props) {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [pseudo, setPseudo] = useState("");
  const [avatar, setAvatar] = useState("");
  const [email, setEmail] = useState("");
  const [nbPosts, setPosts] = useState(0);
  const confirm_mesg = "Do you want to delete your account ? This will remove all user informations but likes and comments will be keept.";
  const confirm_title = "Delete user account";
  const [error, setError] = useState(false);
  const error_msg = getErrorMesg();

  /**
    * Run only after an initial render or after an update on variable
    */
  useEffect(() => {
    getUser();
  }, []);

  /**
   * Api Call : get current user information 
   */
  const getUser = async () => {
    try {
      const access_token = localStorage.getItem("token");
      if(!access_token){
        tokenInvalid();
        props.history.push('/');
      }
      const id = parseJwt(access_token).id;
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/users/" + id
      };
      await axios(options)
        .then((res) => {
          setPseudo(res.data.pseudo);
          setAvatar(res.data.avatar);
          setEmail(res.data.email);
          setPosts(res.data.postsNumber);

        },
          (error) => {
            // Disconnect and return to signin page
            tokenInvalid();
            props.history.push('/');
          });

    } catch (error) {
      // Return to archive page
      props.history.push('/archive');
    }
  }

  /**
  * Api Call : Delete the current account and redirect to home page
  */
  const deleteAccount = async () => {
    try {
      const access_token = localStorage.getItem("token");
      if(!access_token){
        tokenInvalid();
        props.history.push('/');
      }
      const id = parseJwt(access_token).id;
      await axios({
        method: "delete",
        url: getServerUrl() + "/users/" + id,
        headers: {
          Authorization: access_token,
          "Content-Type": "multipart/form-data"
        }
      })
      .then(() => {
        signOut();
      }, (err) => {
          setError(true);
      });
    }
    catch (err) {
      setError(true);
    }
  };

  /**
   * Sign out current user
   */
  const signOut = async () => {
    localStorage.removeItem('token');
    props.history.push('/');
  }

  const changeOpen = async () => {
    setOpen(!open);
  }

  return (
    <React.Fragment>
      <main>
        <Paper className={classes.paper}>
          <div className={classes.postContent}>
            <Container maxWidth="md" >
              {
                error
                  ? <div><Alert name="error" severity="error">{error_msg}</Alert></div>
                  : <div />
              }
              <Grid container spacing={3}>
                <Grid item xs={12} sm={9}>
                  <Typography name="pseudo" variant="h5" align="left" color="dark" paragraph>
                    <Avatar  alt={pseudo} src={avatar} className={classes.large} />
                    {pseudo}
                  </Typography>
                  <Typography name="contact" variant="h9" align="left" color="grey" paragraph>
                    Contact: {email}
                  </Typography>
                  <Typography name="nbPublications" variant="h9" align="left" color="grey" paragraph>
                    Number of publications: {nbPosts ? nbPosts : 0}
                  </Typography>
                  <hr />
                  <Button name="deleteAccount" onClick={changeOpen} align="right" color="primary"><Delete /> Delete account</Button>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <Button name="signout" onClick={signOut} align="right" color="secondary"><ExitToApp /> Sign out</Button>
                </Grid>
              </Grid>
            </Container>
          </div>
        </Paper>
        <Dialog
          open={open}
          onClose={changeOpen}
        >
          {/* Confirm delete account dialog */}
          <DialogTitle>{confirm_title}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              {confirm_mesg}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button name="confirmCancel" onClick={changeOpen} variant="outlined" color="primary">
              No
            </Button>
            <Button name="confirmDelete" onClick={deleteAccount} variant="contained" color="primary" autoFocus>
              Yes
            </Button>
          </DialogActions>
        </Dialog>
      </main>
    </React.Fragment>
  );
}
