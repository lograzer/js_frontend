import React, { useState } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from "@material-ui/core/Container";
import axios from 'axios';
import DropPlace from "../posts/DropPlace";
import Alert from '@material-ui/lab/Alert';
import { useEffect } from 'react';
import parseJwt, { getServerUrl, getErrorMesg, tokenInvalid } from '../../utils/Util';

/**
 * Style of the component
*/
const useStyles = makeStyles(theme => ({
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(2),
    backgroundColor: 'rgba(86, 86, 86, 0.05)'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  previewStyle: {
    display: 'inline',
    width: 100,
    height: 100,
  }
}));

/**
 * Pure fonctionnal component in charge of editing user profil informations
 * @param {*} props 
 */
export default function ProfilEdit(props) {

  const classes = useStyles();
  const edit_str = "Edit profil";
  const description_str = "Choose or drop a picture*";
  const max_length_pseudo = 50;
  const [pseudo, setPseudo] = useState("");
  const [avatar, setAvatar] = useState("");
  const [picture, setPicture] = useState([]);
  const [error, setError] = useState(false);
  let error_msg = getErrorMesg();

  /**
  * Run only after an initial render or after an update on variable
  */
  useEffect(() => {
    getUser();
  }, []);

  /**
   * Function : Return to previous page
   */
  const goBack = () => {
    props.history.push('/archive');
  }

  /**
   * Get selected picture
   * @param picture 
   */
  const getDropResult = async (picture) => {
    setPicture(picture);
  };

  /**
   * Api Call : get current user informations
   */
  const getUser = async () => {
    try {
      const access_token = localStorage.getItem("token");
      if(!access_token){
        tokenInvalid();
        props.history.push('/');
      }
      const id = parseJwt(access_token).id;
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/users/" + id
      };
      let res = await axios(options)
        .then((res) => {
          setAvatar(res.data.avatar);
          setPicture(res.data.avatar);
          setPseudo(res.data.pseudo);
        },
          (error) => {
            // Invalid informations, return to sign in
            tokenInvalid();
            props.history.push('/');

          });

    } catch (error) {
      // Return to archive page
      props.history.push('/archive');
    }
  }

  /**
   * API Call : User update
   */
  const editUser = async () => {
    try {
      const access_token = localStorage.getItem("token");
      if(!access_token){
        tokenInvalid();
        props.history.push('/');
      }
      var bodyFormData = new FormData();
      bodyFormData.set('id', parseJwt(access_token).id);
      bodyFormData.set('pseudo', pseudo);
      bodyFormData.append('picture', picture);

      await axios({
        method: "put",
        data: bodyFormData,
        url: getServerUrl() + "/users/",
        headers: {
          Authorization: access_token,
          "Content-Type": "multipart/form-data"
        }
      })
        .then((response) => {
          // Everything is fine with the request
          setError(false);

        }, (err) => {
          setError(true);
          if (err.response) {
            // The request was made and the server responded with a status code 'error.response'
            if (err.response.status === 403) {
              error_msg = err.response.data.message;
            }
          }
        });
    }
    catch (err) {
      setError(true);
    }
  };

  const submitEdit = async () => {
    await editUser();
    props.history.push('/archive');
  }

  return (
    <React.Fragment>
      <Container maxWidth="sm" className={classes.fragment}>
        <div className={classes.paper}>
          <Avatar alt={pseudo} src={avatar} className={classes.large} />
          <Typography component="h1">
            {edit_str}
          </Typography>
          {
            error
              ? <div><Alert severity="error">{error_msg}</Alert></div>
              : <div />
          }
          <form onSubmit={async (e) => {
            e.preventDefault();
            await submitEdit();
          }}
            className={classes.form}>
            <TextField
              margin="normal"
              required
              fullWidth
              name="pseudo"
              label="Pseudo"
              inputProps={{ maxLength: max_length_pseudo }}
              autoFocus
              type="text"
              value={pseudo}
              onChange={e => setPseudo(e.target.value)}
            />
            <DropPlace message={description_str} callBack={getDropResult}></DropPlace>

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
            >
              Save
                    </Button>
            <Button
              fullWidth
              variant="contained"
              color="default"
              name="goBack"
              onClick={goBack}
            >
              Cancel
                    </Button>
          </form>
        </div>
      </Container>
    </React.Fragment>
  );
}