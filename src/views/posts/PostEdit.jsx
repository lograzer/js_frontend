import React, { useState } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Create from '@material-ui/icons/Create';
import Container from "@material-ui/core/Container";
import Alert from "@material-ui/lab/Alert";
import axios from 'axios';
import { useEffect } from 'react';
import { getServerUrl, getErrorMesg, tokenInvalid } from '../../utils/Util';
/**
 * Style of the component
*/
const useStyles = makeStyles(theme => ({
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(2),
    backgroundColor: 'rgba(86, 86, 86, 0.05)'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  previewStyle: {
    display: 'inline',
    width: 100,
    height: 100,
  }
}));

/**
 * Pur fonctionnal component in charge of post creation.   
 * @param {*parametres} props 
 */
export default function PostEdit(props) {

  const classes = useStyles();
  const { params } = props.match;
  const create_str = "Edit post";
  const max_length_title = 50;
  const max_length_description = 800;
  const [post, setPost] = useState({});
  const [id, setId] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [picture, setPicture] = useState([]);
  const [error, setError] = useState(false);
  let error_msg = getErrorMesg();


  /**
   * Run only after an initial render or after an update on variable
   */
  useEffect(() => {
    getPost();
  }, []);

  /**
  * Api call : get current post informations
  */
  const getPost = async () => {
    try {
      const access_token = localStorage.getItem("token");
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/posts/" + params.id
      };
      await axios(options)
        .then((res) => {
          setPost(res.data);
          setId(res.data._id);
          setTitle(res.data.title);
          setDescription(res.data.description);
          setPicture(res.data.picture);
        },
          (error) => {
            if (error.response) {
              // The request was made and the server responded with a status code 'error.response'
              if (error.response.status === 401) {
                // Invalid token, diconnect and return to signin page
                tokenInvalid();
              }
              // Invalid informations, return to archive
              props.history.push('/archive');
            }
            else {
              // Disconnect and return to signin page
              tokenInvalid();
              props.history.push('/');
            }
          });

    } catch (error) {
      // Return to archive page
      props.history.push('/archive');
    }
  };

  /**
   * Function : Return to previous page
   */
  const goBack = () => {
    props.history.push('/archive');
  }

  /**
   * API Call : creation d'un post
   */
  const editPost = async () => {
    try {
      const access_token = localStorage.getItem("token");
      const values = {
        'id': params.id,
        'title': title,
        'description': description,
        'picture': picture
      };

      await axios.put(getServerUrl() + "/posts", values,
        {
          headers: {
            Authorization: access_token,
            "content-type": "application/json"
          }
        })
        .then((res) => {
          setError(false);
          error_msg = getErrorMesg();
        },
          (err) => {
            setError(true);
          });
    }
    catch (err) {
      setError(true);
    }
  };

  /**
    * Function : Control inputs and try to create a post
    * @param {*} title 
    * @param {*} description 
    * @param {*} picture 
    * @param {*} props 
    */
  const submitEdit = async () => {
    await editPost();
    if(!error){
      props.history.push('/archive');
    }   
  }

  return (
    <React.Fragment>
      <Container maxWidth="sm" className={classes.fragment}>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <Create />
          </Avatar>
          <Typography component="h1">
            {create_str}
          </Typography>
          {/* Error message */}
          {
            error
              ? <div><Alert severity="error">{error_msg}</Alert></div>
              : null
          }
          <form onSubmit={async (e) => {
            e.preventDefault();
            await submitEdit();
          }}
            className={classes.form}>
            <TextField
              margin="normal"
              required
              fullWidth
              label="Title"
              inputProps={{ maxLength: max_length_title }}
              autoFocus
              type="text"
              name="title"
              value={title}
              onChange={e => setTitle(e.target.value)}
            />
            <TextField
              multiline
              margin="normal"
              required
              fullWidth
              label="Description"
              name="description"
              type="textarea"
              inputProps={{ maxLength: max_length_description }}
              value={description}
              onChange={e => setDescription(e.target.value)}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="secondary"
              className={classes.submit}
            >
              Save
                    </Button>
            <Button
              fullWidth
              variant="contained"
              color="default"
              name="cancelEdit"
              onClick={goBack}
            >
              Cancel
                    </Button>
          </form>
        </div>
      </Container>
    </React.Fragment>
  );
}