import React, { useState } from 'react'
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Add from '@material-ui/icons/Add';
import Container from "@material-ui/core/Container";
import axios from 'axios';
import DropPlace from "./DropPlace";
import Alert from '@material-ui/lab/Alert';
import { getServerUrl, tokenInvalid, getErrorMesg } from '../../utils/Util';
/**
 * Style of the component
*/
const useStyles = makeStyles(theme => ({
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(2),
        backgroundColor: 'rgba(86, 86, 86, 0.05)'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    previewStyle: {
        display: 'inline',
        width: 100,
        height: 100,
    }
}));

/**
 * Pur fonctionnal component in charge of post creation.   
 * @param {*parametres} props 
 */
export default function PostCreate(props) {

    const classes = useStyles();
    const create_str = "Create post";
    const description_str = "Choose or drop a picture*";
    const max_length_title = 50;
    const max_length_description = 800;
    const [error, setError] = useState(false);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [picture, setPicture] = useState(null);
    const [error_msg, setErrorMesg] = useState(getErrorMesg());

    /**
     * Function : Return to previous page
     */
    const goBack = () => {
        props.history.push('/archive');
    }

    /**
     * Recuperation de l'image selectionnee
     * @param picture 
     */
    const getDropResult = async (picture) => {
        setPicture(picture);
    };

    /**
     * API Call : post creation
     */
    const createPost = async () => {
        try {
            if (picture) {
                const access_token = localStorage.getItem("token");
                var bodyFormData = new FormData();
                bodyFormData.set('title', title);
                bodyFormData.set('description', description);
                bodyFormData.append('picture', picture);

                await axios({
                    method: "post",
                    data: bodyFormData,
                    url: getServerUrl() + "/posts",
                    headers: {
                        Authorization: access_token,
                        "Content-Type": "multipart/form-data"
                    }
                })
                    .then((response) => {
                        // Everything is fine with the request
                        setError(false);
                        setErrorMesg(getErrorMesg());
                    }, (err) => {
                        if (err.response) {
                            // The request was made and the server responded with a status code 'error.response'
                            if (err.response.status === 400 || err.response.status === 422) {
                                setErrorMesg(err.response.data.message);
                            }
                            setError(true);
                        }
                        else {
                            // Something happened in setting up the request that triggered an Error 'error.message'
                            tokenInvalid();
                            props.history.push('/');
                        }
                    });
            }
            else {
                setError(true);
                setErrorMesg("Please choose a picture.");
            }
        }
        catch (err) {
            tokenInvalid();
            props.history.push('/');
        }
    };

    /**
      * Function : Control inputs and try to create a post
      * @param {*} title 
      * @param {*} description 
      * @param {*} picture 
      * @param {*} props 
      */
    const submitCreation = async () => {
        await createPost();
        if(error){
            props.history.push('/archive');
        }
    }


    return (
        <React.Fragment>
            <Container maxWidth="sm" className={classes.fragment}>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <Add />
                    </Avatar>
                    <Typography component="h1">
                        {create_str}
                    </Typography>
                    {
                        error
                            ? <div><Alert name="error" severity="error">{error_msg}</Alert></div>
                            : <div />
                    }
                    <form onSubmit={async (e) => {
                        e.preventDefault();
                        await submitCreation();
                    }}
                        className={classes.form}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            label="Title"
                            inputProps={{ maxLength: max_length_title }}
                            autoFocus
                            type="text"
                            name="title"
                            value={title}
                            onChange={e => setTitle(e.target.value)}
                        />
                        <TextField
                            multiline
                            margin="normal"
                            required
                            fullWidth
                            label="Description"
                            name="description"
                            type="textarea"
                            inputProps={{ maxLength: max_length_description }}
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                        />

                        <DropPlace message={description_str} callBack={getDropResult}></DropPlace>

                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                        >
                            Create
                    </Button>
                        <Button
                            fullWidth
                            variant="contained"
                            color="default"
                            name="cancelCreate"
                            onClick={goBack}
                        >
                            Cancel
                    </Button>
                    </form>
                </div>
            </Container>
        </React.Fragment>
    );
}