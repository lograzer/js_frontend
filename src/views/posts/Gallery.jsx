import React, { useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
import { CardActions, Link } from '@material-ui/core';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CommentIcon from '@material-ui/icons/Comment';
import Badge from '@material-ui/core/Badge';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import CreateIcon from '@material-ui/icons/Create';
import Delete from '@material-ui/icons/Delete';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import parseJwt, { getServerUrl, getErrorMesg, tokenInvalid } from '../../utils/Util';
/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
  postContent: {
    padding: theme.spacing(2),
    color: 'white',
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  },
  postButtons: {
    marginTop: theme.spacing(4),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  root: {
    flexGrow: 1,
  },
  element: {
    maxWidth: 345,
    height: '100%'
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  loader: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    marginTop: theme.spacing(5)
  },
  totop: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    position: 'fixed',
    right: 10,
    bottom: 10
  },
  create: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)'
  },
  edit: {
    margin: theme.spacing(1),
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)'
  },
  delete: {
    margin: theme.spacing(1),
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)'
  },
  paragraph: {
    align: 'center'
  }
}));


/**
 * Pure fonctionnal component in charge of displaying random posts as cards.
 * @param {*} props 
 */
export default function Home(props) {

  // Variables
  const classes = useStyles();
  const [posts, setPosts] = useState([]);
  const [start_posts, setStart] = useState(0);
  const [disable, setDisable] = useState(false);
  const create_post_str = "New Post";
  const introduction_str = "Check and organize all your publications. Express your fantasy  and share your ideas!";
  let maximum_posts = 8;
  const no_post_str = "No post to display.";
  const [error, setError] = useState(false);
  const error_msg = getErrorMesg();

  /**
   * Run only after an initial render or after an update on variable
   */
  useEffect(() => {
    getAllPosts();
  }, []);



  /**
   * Api call : get all posts from current user
   */
  const getAllPosts = async () => {
    try {
      const access_token = localStorage.getItem("token");
      if(!access_token){
        tokenInvalid();
        props.history.push('/');
      }
      const user_id = parseJwt(access_token).id;
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/users/" + user_id + "/posts?limit=" + maximum_posts + "&start=" + start_posts
      };
      await axios(options)
        .then((res) => {
          let all_posts = posts.concat(res.data.posts);
          setPosts(all_posts);
          if (res.data.posts.length === 0 || res.data.posts.length < maximum_posts) {
            setDisable(true);
          }
          setStart(start_posts + maximum_posts);
        },
          (error) => {
            if (error.response) {
              // The request was made and the server responded with a status code 'error.response'
              if (error.response.status === 401) {
                // Invalid token, diconnect and return to signin page
                tokenInvalid();
              }
              // Invalid informations, return to homepage
              props.history.push('/');
            }
            else {
              // Disconnect and return to signin page
              tokenInvalid();
              props.history.push('/');
            }
          });

    } catch (error) {
      // Return to home page
      props.history.push('/');
    }
  };

  /**
     * Api call : delete a specific post
     * @param {*} id 
     */
  const deletePost = async (id) => {
    try {
      const access_token = localStorage.getItem("token");
      await axios.delete(getServerUrl() + "/posts/" + id,
        {
          headers: {
            Authorization: access_token,
            "content-type": "application/json"
          }
        })
        .then(() => {
          setError(false);
        }, (err) => {
          if (err.response.status !== 400) {
            setError(true);
          }
        });
    }
    catch (err) {
      setError(true);
    }
  }

  /**
   * Handle deletion of the selected post
   * @param {*} id 
   */
  const submitDelete = async (id) => {
    await deletePost(id);
    if(!error){
      var filteredPost = posts.filter(function (p) {
        return p._id !== id;
      });
      setPosts(filteredPost);
    } 
  }

  /**
   * Get path to edit the selected post
   * @param {*} id 
   */
  const getEditLink = (id) => {
    return "/post/edit/" + id;
  }

  /**
   * Api call : Load more posts
   */
  const loadMorePost = async () => {
    getAllPosts();
  }

  /**
   * Enable user to come back to the top of the current page
   */
  const scrollToTop = async () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }

  /**
  * Get path to detail view
  * @param {*} id 
  */
  const getDetailLink = (id) => {
    return "/post/" + id;
  }

  /**
  * Get path to author archives
  * @param {*} id 
  */
  const getAuthorArchive = (id) => {
    return "/user/" + id + "/archive";
  }

  return (
    <React.Fragment>
      <main>
        {/* Titre */}
        <div className={classes.postContent}>
          <Container maxWidth="md">
            <Typography variant="h6" align="center" paragraph>
              {introduction_str}
            </Typography>
            <div className={classes.postButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button name="createPost" className={classes.create} variant="contained" color="secondary" href="/post/create">
                    <AddBox /> {create_post_str}
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        {/* Error message */}
        {
          error
            ? <div><Alert severity="error">{error_msg}</Alert></div>
            : null
        }
        <hr />
        {/* Posts */}
        <div className={classes.root}>
          <Grid container spacing={3}>
            {posts.length === 0
              ? <Grid item><Typography name="noPosts">{no_post_str}</Typography></Grid>
              :
              Array.from(posts).map(post => (
                <Grid item key={post} xs={6} sm={3}>
                  <Card className={classes.element}>
                    <CardHeader
                      avatar={
                        <Link class="user-archive" href={getAuthorArchive(post.author._id)}>
                          <Avatar alt={post.author.pseudo} src={post.author.avatar} className={classes.large} />
                        </Link>
                      }
                      action={
                        <div>
                          <IconButton aria-label="Delete" onClick={() => submitDelete(post._id)} className={classes.delete} size="small">
                            <Delete />
                          </IconButton>
                          <IconButton aria-label="Edit" href={getEditLink(post._id)} className={classes.edit} size="small">
                            <CreateIcon />
                          </IconButton>
                        </div>
                      }
                      title={post.title.length > 20 ? post.title.substring(0, 17) + "..." : post.title}
                      subheader={post.author.pseudo}
                    />
                    <Link class="post-image" href={getDetailLink(post._id)}>
                      <CardMedia
                        className={classes.media}
                        image={post.picture.replace("public\\uploads\\", "")}
                        title={post.title}
                      />
                    </Link>
                    <CardActions >
                      <Badge badgeContent={post.likesNumber} color="secondary">
                        <FavoriteIcon />
                      </Badge>
                      <Badge badgeContent={post.commentsNumber} color="primary">
                        <CommentIcon />
                      </Badge>
                    </CardActions>
                  </Card>
                </Grid>
              ))
            }
          </Grid>
        </div>
        <Button name="totop" className={classes.totop} variant="contained" position="absolute" color="secondary" onClick={scrollToTop}>
          <ArrowUpward />
        </Button>
        <Grid container justify="center" >
          <Grid item>
            <Button name="loadMore" justify="center" className={classes.loader} variant="contained" color="secondary" size="large" disabled={disable} onClick={loadMorePost}>
              Load more
            </Button>
          </Grid>
        </Grid>
      </main>
    </React.Fragment>
  );
}