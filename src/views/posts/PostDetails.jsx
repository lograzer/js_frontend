import React, { useState, useEffect } from 'react'
import LikeDetails from '../comments/LikeDetails';
import CommentDetails from '../comments/CommentDetails';
import Button from '@material-ui/core/Button';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import axios from 'axios';
import { Paper } from "@material-ui/core";
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import ZoomOutMap from '@material-ui/icons/ZoomOutMap';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { getServerUrl, tokenInvalid, getErrorMesg } from '../../utils/Util';
import Alert from '@material-ui/lab/Alert';

/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
    postContent: {
        padding: theme.spacing(2),
        color: 'black',
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    paper: {
        padding: theme.spacing(2),
        margin: "auto",
        maxWidth: 900
    },
    postButtons: {
        marginTop: theme.spacing(4),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    root: {
        flexGrow: 1,
        margin: theme.spacing(2)
    },
    element: {
        maxWidth: 345
    },
    media: {
        height: 0,
        paddingTop: "56.25%" // 16:9
    },
    totop: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        position: 'fixed',
        right: 10,
        bottom: 10
    },
    image: {
        width: 128,
        height: 128
    },
    header: {
        top: 0
    },
    check: {
        height: 180
    },
    imageOpen: {
        height: "80vh"
    },
    dialog: {
        width: "md",
        fullWidth: true
    }
}));

/**
 * Pur fonctionnal component in charge of post details display.   
 * @param {*parametres} props 
 */
export default function PostDetails(props) {

    const { params } = props.match;
    const classes = useStyles();

    const [open, setIsOpen] = useState(false);
    const [id, setId] = useState("");
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [comment, setComment] = useState("");
    const [picture, setPicture] = useState("");
    const [author, setAuthor] = useState({});
    const [error, setError] = useState(false);
    const error_msg = getErrorMesg();

    const [checkedLike, setCheckedLike] = useState(false);
    const [checkedComment, setCheckedComment] = useState(true);

    const max_length = 800;

    /**
     * Run only after an initial render or after an update on variable
     */
    useEffect(() => {
        getPost();
    }, []);

    /**
     * Api call : get all posts from every users
     */
    const getPost = async () => {
        try {
            const access_token = localStorage.getItem("token");
            const options = {
                method: "get",
                headers: {
                    Authorization: access_token,
                    "Content-Type": "application/json"
                },
                url: getServerUrl() + "/posts/" + params.id
            };
            await axios(options)
                .then((res) => {
                    setError(false);
                    setId(res.data._id);
                    setTitle(res.data.title);
                    setDescription(res.data.description);
                    setPicture(res.data.picture);
                    setAuthor(res.data.author);
                },
                    (error) => {
                        if (error.response) {
                            // The request was made and the server responded with a status code 'error.response'
                            if (error.response.status === 401) {
                                // Invalid token, diconnect and return to signin page
                                tokenInvalid();
                            }
                            // Invalid informations, return to archive
                            props.history.push('/archive');
                        }
                        else {
                            // Disconnect and return to signin page
                            tokenInvalid();
                            props.history.push('/');
                        }
                    });

        } catch (error) {
            // Return to archive page
            props.history.push('/archive');
        }
    };

    /**
    * API Call : comment creation
    */
    const createComment = async () => {
        try {
            const access_token = localStorage.getItem("token");
            const values = { 'message': comment };
            await axios.post(getServerUrl() + "/posts/" + id + "/comments", values,
                {
                    headers: {
                        Authorization: access_token,
                        "content-type": "application/json"
                    }
                })
                .then((res) => {
                    setError(false);
                },
                    (err) => {
                        setError(true);
                    });
        } catch (err) {
            setError(true);
        }
    };

    /**
     * Handle creation of a comment
     */
    const submitComment = async () => {
        await createComment();
        setComment("");
        setCheckedComment(false);
        setCheckedComment(true);
    }

    /**
   * Enable user to come back to the top of the current page
   */
    const scrollToTop = async () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }

    /**
     * Open and close Dialog item
     */
    const changeOpen = () => {
        setIsOpen(!open);
    }

    return (
        <React.Fragment>
            <main>
                <Paper className={classes.paper}>
                    {/* Error message */}
                    {
                        error
                            ? <div><Alert name="error" severity="error">{error_msg}</Alert></div>
                            : null
                    }
                    {/* Post details */}
                    <div className={classes.postContent}>
                        <Container maxWidth="md" >
                            <Grid container spacing={3}>
                                <Grid item xs={12} sm={10}>
                                    <ListItem>
                                        <ListItemAvatar>
                                            <Avatar alt={author.pseudo} src={author.avatar} className={classes.large} />
                                        </ListItemAvatar>
                                        <ListItemText name="title" primary={title} secondary={author.pseudo} />
                                    </ListItem>
                                </Grid>
                                <Grid item xs={12} sm={2} align="right">
                                    <IconButton onClick={changeOpen} align="right">
                                        <ZoomOutMap />
                                    </IconButton>
                                </Grid>
                            </Grid>
                            <CardMedia
                                className={classes.media}
                                image={picture.replace("public\\uploads\\", "")}
                                title={title}
                            />
                            <Typography variant="h8" align="left" color="dark" paragraph>
                                {description}
                            </Typography>

                            {/* Comment */}
                            <hr />
                            <form onSubmit={async (e) => {
                                e.preventDefault();
                                await submitComment();
                            }}
                                className={classes.form}>
                                <TextField
                                    multiline
                                    margin="normal"
                                    required
                                    fullWidth
                                    label="Comment"
                                    type="textarea"
                                    name="commentText"
                                    inputProps={{ maxLength: max_length }}
                                    value={comment}
                                    onChange={e => setComment(e.target.value)}
                                />
                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}
                                >
                                    Comment
                        </Button>
                            </form>
                        </Container>
                    </div>

                    {/* Likes and comments */}
                    <div className={classes.root}>
                        <Container maxWidth="md">
                            <FormControlLabel
                                maxWidth="md"
                                control={<Switch  name="displayLikes" checked={checkedLike} onClick={() => setCheckedLike(!checkedLike)} />}
                                label="Show likes"
                            />
                            {
                                checkedLike
                                    ? <LikeDetails id={params.id} visible={checkedLike} setError={setError}></LikeDetails>
                                    : <div></div>
                            }
                            {
                                checkedComment
                                    ? <CommentDetails id={params.id} author={author._id} setError={setError}></CommentDetails>
                                    : <div></div>
                            }
                        </Container>
                    </div>
                </Paper>
                <Button className={classes.totop} variant="contained" position="absolute" color="secondary" onClick={scrollToTop}>
                    <ArrowUpward />
                </Button>
                {/* Dialog enlarge picture */}
                <Dialog
                    open={open}
                    onClose={changeOpen}
                    className={classes.dialog}
                >
                    <DialogContent>
                        <img
                            className={classes.imageOpen}
                            src={picture.replace("public\\uploads\\", "")}
                            alt={title}
                        />
                    </DialogContent>
                </Dialog>
            </main>
        </React.Fragment>
    );

}
