import React, { useState }  from 'react';
import {useDropzone} from 'react-dropzone';
import Paper from '@material-ui/core/Paper';
import Cancel from '@material-ui/icons/Cancel';
import Button from '@material-ui/core/Button';

/**
 * Pure fonctionnal component in charge of receiving picture file choice   
 * @param {*parametres} props 
 */
export default function DropPlace(props) {

    const [change, setChange] = useState(true);

    /**
     * Function : Update picture to send to the API 
     * @param acceptedFiles 
     */
    const callBack = async(acceptedFiles) =>{
        await props.callBack(acceptedFiles[0]);
    };

    /**
     *  Constructor : Create and set options for the dropzone element
     */
    const {acceptedFiles, getRootProps, getInputProps} = useDropzone({
        accept: 'image/jpg, image/jpeg, image/png',
        multiple: false,
        onDrop: callBack
    });


    const removeFiles = () => () => {
        acceptedFiles.splice(0,acceptedFiles.length);
        setChange(!change);
    }

    /**
     * Function : Display the selected files
     * */
    const acceptedFilesItems = acceptedFiles.map(file => (
        <li key={file.path}>
        You choosed {file.path.length > 30 ? file.path.substring(0, 27) + "..." : file.path} - {file.size} bytes 
        <Button type='button' onClick={removeFiles()}>
            <Cancel/>
        </Button>
        </li>
    ));

    return (
        <Paper variant="outlined" className="mt-3">
            <section className="container">
                <div {...getRootProps({className: 'dropzone'})}>
                    <input {...getInputProps()} />
                    <p></p>
                    <p>{props.message}</p>
                    <p>(Only *.jpeg, *jpg and *.png images accepted)</p>
                </div>
                <aside className="mb-3">
                    {acceptedFilesItems}
                </aside>
            </section> 
        </Paper>       
    );
}
