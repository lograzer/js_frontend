import React, { useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from "@material-ui/core/CardHeader";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';
import { CardActions, Link } from "@material-ui/core";
import { getServerUrl, tokenInvalid, getErrorMesg } from "../../utils/Util";

/**
 * Style of the component 
*/
const useStyles = makeStyles(theme => ({
  postContent: {
    padding: theme.spacing(2),
    color: 'white',
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center'
  },
  postButtons: {
    marginTop: theme.spacing(4),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  root: {
    flexGrow: 1,
  },
  element: {
    maxWidth: 345
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  loader: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    marginTop: theme.spacing(5)
  },
  totop: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    position: 'fixed',
    right: 10,
    bottom: 10
  },
  create: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)'
  },
  gallery: {
    background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)'
  }
}));

/**
 * Pure fonctionnal component in charge of displaying all posts of a specific user.
 * @param {*} props 
 */
export default function Archive(props) {

  const { params } = props.match;
  const classes = useStyles();

  const [posts, setPosts] = useState([]);
  const [start_posts, setStart] = useState(0);
  const [disable, setDisable] = useState(false);
  const [liked, setLiked] = useState(new Map()); // key=id, value={key=boolean, value=strings}
  const gallery_str = "Archive";
  const create_post_str = "New Post";
  const introduction_str = "Check, like and explore the galleries of the other InstaZZers ! Find inspiration or share your experience with the community.";
  let maximum_posts = 8;
  const no_post_str = "No post to display.";
  const [error, setError] = useState(false);
  const error_msg  = getErrorMesg();
  /**
   * Run only after an initial render or after an update on variable
   */
  useEffect(() => {
    getAllPosts();
  }, []);

  /**
  * Api call : get all posts from user
  */
  const getAllPosts = async () => {
    try {
      const access_token = localStorage.getItem("token");
      const user_id = params.id;
      const options = {
        method: "get",
        headers: {
          Authorization: access_token,
          "Content-Type": "application/json"
        },
        url: getServerUrl() + "/users/" + user_id + "/posts?limit=" + maximum_posts + "&start=" + start_posts
      };
      await axios(options)
        .then((res) => {
          let all_posts = posts.concat(res.data.posts);
          setPosts(all_posts);
          if (res.data.posts.length === 0 || res.data.posts.length < maximum_posts) {
            setDisable(true);
          }
          setStart(start_posts + maximum_posts);
          initLikeColors(res.data);
        },
          (error) => {
            if (error.response) {
              // The request was made and the server responded with a status code 'error.response'
              if (error.response.status === 401) {
                // Invalid token, diconnect and return to signin page
                tokenInvalid();
              }
              // Invalid informations, return to homepage
              props.history.push('/');
            }
            else {
              // Disconnect and return to signin page
              tokenInvalid();
              props.history.push('/');
            }
          });

    } catch (error) {
      // Return to home page
      props.history.push('/');
    }
  };

  /**
 * Api call : Like a post
 * @param {} id 
 */
  const like = async (id) => {
    try {
      const access_token = localStorage.getItem("token");
      await axios({
        method: "post",
        url: getServerUrl() + "/posts/" + id + "/likes",
        headers: {
          Authorization: access_token,
          "Content-Type": "multipart/form-data"
        }
      })
      .then(() => {
        setError(false);
      }, (err) => {
        if(err.response.status !== 400){
          setError(true);
        }
      });
    } 
    catch (err) {
      setError(true);
    }
  }

  /**
   * Api call : Dislike a post
   * @param {} id 
   */
  const dislike = async (id) => {
    try {
      const access_token = localStorage.getItem("token");

      await axios({
        method: "delete",
        url: getServerUrl() + "/posts/" + id + "/likes",
        headers: {
          Authorization: access_token,
          "Content-Type": "multipart/form-data"
        }
      })
      .then(() => {
        setError(false);
      }, (err) => {  
        if(err.response.status !== 400){
        setError(true);
      }
      });
    } 
    catch (err) {
      setError(true);
    }
  }

  /**
   * Initialize dictionnaries for like buttons color and their boolean
   */
  const initLikeColors = async (posts) => {
    let newLiked = new Map();
    Array.from(posts).map(post => {
      if (!newLiked.has(post._id)) {
        newLiked.set(post._id, { liked: false, color: "" });
      }
    }
    );
    setLiked({ ...newLiked, ...liked });
  }

  /**
   * Update if a post is liked / disliked
   * @param {*} id 
   */
  const updateLike = async (id) => {

    if (liked[id] !== undefined && liked[id].liked) {
      setLiked({ ...liked, [id]: { liked: false, color: "" } });
      dislike(id);
    }
    else {
      setLiked({ ...liked, [id]: { liked: true, color: "secondary" } });
      like(id);
    }
  }

  /**
   * Api call : Load more posts
   */
  const loadMorePost = async () => {
    getAllPosts();
  }

  /**
   * Enable user to come back to the top of the current page
   */
  const scrollToTop = async () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    });
  }

  /**
   * Get path to detail view
   * @param {*} id 
   */
  const getDetailLink = (id) => {
    return "/post/" + id;
  }

  /**
  * Get path to author archives
  * @param {*} id 
  */
  const getAuthorArchive = (id) => {
    return "/user/" + id + "/archive";
  }


  return (
    <React.Fragment>
      <main>
        {/* Title */}
        <div className={classes.postContent}>
          <Container maxWidth="md" >
            <Typography variant="h6" align="center" paragraph>
              {introduction_str}
            </Typography>
            <div className={classes.postButtons}>
              <Grid container spacing={2} justify="center">
                <Grid item>
                  <Button name="accessArchive" className={classes.gallery} variant="contained" color="primary" href="/archive">
                    {gallery_str}
                  </Button>
                </Grid>
                <Grid item>
                  <Button name="createPost" className={classes.create} variant="contained" color="secondary" href="/post/create">
                    <AddBox /> {create_post_str}
                  </Button>
                </Grid>
              </Grid>
            </div>
          </Container>
        </div>
        {/* Error message */}
        {
          error
            ? <div><Alert severity="error">{error_msg}</Alert></div>
            : null
        }
        <hr />
        {/* Posts */}
        <div className={classes.root}>
          <Grid container spacing={3}>
            {posts.length === 0
              ? <Grid item><Typography name="noPosts">{no_post_str}</Typography></Grid>
              :
              Array.from(posts).map(post => (
                <Grid item key={post} xs={6} sm={3}>
                  <Card className={classes.element}>
                    <CardHeader
                      avatar={
                        <Link class="user-archive" href={getAuthorArchive(post.author._id)}>
                          <Avatar alt={post.author.pseudo} src={post.author.avatar} className={classes.large} />
                        </Link>
                      }
                      action={
                        <IconButton onClick={() => updateLike(post._id)} title="Like">
                          <FavoriteIcon color={liked[post._id] ? liked[post._id].color : ""} />
                        </IconButton>
                      }
                      title={post.title.length > 30 ? post.title.substring(0, 27) + "..." : post.title}
                      subheader={post.author.pseudo}
                    />
                    <Link class="post-image" href={getDetailLink(post._id)}>
                      <CardMedia
                        className={classes.media}
                        image={post.picture.replace("public\\uploads\\", "")}
                        title={post.title}
                      />
                    </Link>
                    <CardActions />
                  </Card>
                </Grid>
              ))
            }
          </Grid>
        </div>
        {/* Go back to the top */}
        <Button name="totop" className={classes.totop} variant="contained" position="absolute" color="secondary" onClick={scrollToTop}>
          <ArrowUpward />
        </Button>
        {/* Load more posts */}
        <Grid container justify="center" >
          <Grid item>
            <Button name="loadMore" justify="center" className={classes.loader} variant="contained" color="secondary" size="large" disabled={disable} onClick={loadMorePost}>
              Load more
            </Button>
          </Grid>
        </Grid>
      </main>
    </React.Fragment>
  );
}