
/**
 * Get elements from the given token
 * @param {*} token 
 */
export default function parseJwt(token) {

    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
}

/**
 * Return server url
 */
export function getServerUrl() {
   return "https://instazz-back.herokuapp.com/api/v1";
}

/**
 * Remove current user token
 */
export function tokenInvalid() {
    localStorage.removeItem("token");
}

/**
 * 
 */
export function getErrorMesg(){
    return "An error occured. Please retry or verify your internet connection.";
}