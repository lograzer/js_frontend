import React, {useState}  from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Gallery from './views/posts/Gallery';
import Home from './views/posts/Home';
import Archive from './views/posts/Archive';
import SignIn from './views/connection/SignIn';
import PostCreate from './views/posts/PostCreate';
import PostDetails from './views/posts/PostDetails';
import PostEdit from './views/posts/PostEdit';
import ProfilDetails from './views/users/ProfilDetails';
import ProfilEdit from './views/users/ProfilEdit';

export default function Routes(props) {

  const [tokenExists, setTokenExists] = useState(false);

    const handleLogin = async() =>  props.handleLogin();
    
    /**
     * Function : A remplacer (on verifie le bon token a chaque requete)
     */
    const checkLoginStatus = () => {     
       try{
          const token = localStorage.getItem('token');
          if(token){
            setTokenExists(true);
            props.setUserLoggedIn(true);
          }
          else {
            setTokenExists(false);
            localStorage.removeItem('token');
            props.setUserLoggedIn(false);
          }
      }
      catch(error){
      }
    }
    /**
     * Gestion des redirections sur chaque page en fonction de si l'utilisateur est connecté ou pas
     */
    const PrivateRoute = ({ ...props }) => {
      checkLoginStatus();
      if(props.isUserLoggedIn === true){
        return <Route { ...props } />
      }
      else{
        return <Redirect to="/signin"  {...props}/>
      }
    }    

    const LoginRoute = ({ ...props}) => {
      checkLoginStatus();
      if(props.isUserLoggedIn === true){
        return <Redirect to="/"  {...props}/>
      }
      else{
        return <Route {...props}/>
      }
    }     

    return (      
        <Switch>
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } exact path='/' component={Home} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } path="/archive" component={Gallery} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } path="/user/:id/archive" component={Archive} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } path="/post/create" component={PostCreate} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } exact path="/post/:id" component={PostDetails} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } exact path="/post/edit/:id" component={PostEdit} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } exact path="/user/details" component={ProfilDetails} />
          <PrivateRoute isUserLoggedIn={ props.isUserLoggedIn } exact path="/user/edit" component={ProfilEdit} />
          <LoginRoute path="/signin" component={() => <SignIn handleLogin={handleLogin} tokenExists={tokenExists} />} />
          <LoginRoute component={() => <SignIn handleLogin={handleLogin} tokenExists={tokenExists} />} />
        </Switch>
    );
}