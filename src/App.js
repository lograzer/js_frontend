import './App.css';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { BrowserRouter as Router } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import Routes from './Routes';
import Footer from './views/shared/Footer';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Settings from '@material-ui/icons/Settings';
import Panorama from '@material-ui/icons/Panorama';
import PhotoLibrary from '@material-ui/icons/PhotoLibrary';
import Instagram from '@material-ui/icons/Instagram';

/**
 * Style of the component
 */
const useStyles = makeStyles(theme => ({
  root: {
    background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
    color: 'white'
  }
}));

/**
 * Pure fonctionnal component in charge  du lancement de l'application.  
 */
export default function App() {
  const classes = useStyles();
  const [isUserLoggedIn, setUserLoggedIn] = useState(true);

  /**
   * Function : Update user status on log in success
   */
  const handleLogin = async () => {
    setUserLoggedIn(true);
  };

  return (
    // Menu de navigation
    <Router>
      { isUserLoggedIn
        ?
        <Navbar collapseOnSelect expand="lg" className={classes.root} bg="dark" variant="dark">
          <Navbar.Brand href="/">
            <Instagram /> InstaZZ
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/"><Panorama /> Gallery</Nav.Link>
              <Nav.Link href="/archive"><PhotoLibrary /> Archive</Nav.Link>
            </Nav>
            <Nav>
              <Nav.Link href="/user/edit"><Settings /> Edit profil</Nav.Link>
              <Nav.Link href="/user/details"><AccountCircle /> My account</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        :
        null
      }
      <Routes handleLogin={handleLogin} isUserLoggedIn={isUserLoggedIn} setUserLoggedIn={setUserLoggedIn} />
      {
        isUserLoggedIn
          ? <Footer></Footer>
          : null
      }
    </Router>
  );
}

