# InstaZZ Front-End - project bootstrapped with [create React App](https://github.com/facebook/create-react-app).
## Authors: Louise GRÄZER & Lou GRUNNER (F2)
* Access InstaZZ application on https://instazz-front.herokuapp.com/
* Tests realized with the JavaScript **End to End Testing Framework Cypress** and the **Istanbul test code coverage tool**. 
* Application is hosted on the **Cloud Application Platform Heroku**.


## 1. Code coverage:
Running tests with:
```
npm run cypress:open
```
![Code coverage of the application under 'cypress/fixtures/pictures/'](/cypress/fixtures/pictures/code_coverage_front_end.JPG)

## 2. Functionalities:

### `Users`
* Sign up to application
* Log in to application
* Log out to application
* Delete current user account
* Edit pseudo and profil picture of current user
* Access details of current profil informations  
### `Posts`
* Access randoms posts (home)
* Access posts of a selected user (archive->user)
* Access posts of current user (archive->current user) with edit access
* Add post
* Access details of a post
* Open/enlarge picture of a post
* Delete post
* Edit title and description of a post
* Like/ Dislike a post
* Add comment to a post
* Delete comment from belonging posts
* Delete comment from current user


## Available Scripts:

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
